/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.util;

import static java.lang.Integer.max;

import br.usp.ime.owl2dlstratification.strata.Strata;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import org.semanticweb.owlapi.model.OWLAxiom;

public class DummyStrata implements Strata<OWLAxiom> {

  final Set<OWLAxiom> ontology;

  public DummyStrata(Set<OWLAxiom> ontology) {
    this.ontology = ontology;
  }

  @Override
  public Optional<Set<OWLAxiom>> getLayer(int rank) {
    switch (rank) {
      case 0:
        return Optional.ofNullable(ontology);
      case 1:
        return Optional.of(new HashSet<>());
      default:
        return Optional.empty();
    }
  }

  @Override
  public int size() {
    return 0;
  }

  private List<Set<OWLAxiom>> getLayers(int begin, int end) {
    List<Set<OWLAxiom>> slice = Lists
        .newArrayListWithCapacity(max(begin - end + 1, 0));

    for (int i = begin; i <= end; i++) {
      getLayer(i).ifPresent(slice::add);
    }

    return slice;
  }

  @Override
  public Optional<Integer> getRank(OWLAxiom axiom) {
    return ontology.contains(axiom) ? Optional.of(1) : Optional.empty();
  }

  @Nonnull
  @Override
  public Iterator<Set<OWLAxiom>> reverseIterator() {
    List<Set<OWLAxiom>> reversed = new ArrayList<>(2);
    reversed.add(this.getLayer(1).orElse(new HashSet<>()));
    reversed.add(this.getLayer(0).orElse(new HashSet<>()));
    return reversed.iterator();
  }

  @Nonnull
  @Override
  public Iterator<Set<OWLAxiom>> iterator() {

    return Stream.of(0, 1).map(this::getLayer).filter(Optional::isPresent).map(Optional::get)
        .collect(Collectors.toList()).iterator();

  }
}
