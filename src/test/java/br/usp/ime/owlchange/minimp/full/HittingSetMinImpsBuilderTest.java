/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.minimp.full;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import br.usp.ime.owlchange.TestCaseUtils;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.BlackBoxMinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.ClassicalMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.MinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.TrivialMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.ClassicalMinImpShrinker;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.MinImpShrinker;
import com.google.common.collect.Sets;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class HittingSetMinImpsBuilderTest extends MinImpsBuilderTest {

  @Override
  void emptyOntologyNoMinImpForTautology() {
    MinImpBuilder mockedMinImpBuilder = mock(MinImpBuilder.class);
    when(mockedMinImpBuilder.minImp(any(), any())).thenReturn(
        java.util.Optional.of(Sets.newHashSet()));

    HittingSetMinImpsBuilder minImpsBuilder = new HittingSetMinImpsBuilder(
        mockedMinImpBuilder);

    emptyOntologyNoMinImpForTautology(minImpsBuilder);
  }

  private static Stream<Arguments> ontologyAndAnySubsumptionProvider() {
    return Stream.of("accounts.owl")
        .map(path -> TestCaseUtils.loadOntology(HittingSetMinImpsBuilderTest.manager, path)).map(
            ontology -> Arguments.of(
                ontology.axioms().collect(Collectors.toSet()),
                Sets.newHashSet(TestCaseUtils.getAnySubsumption(ontology)))
        );
  }

  @Override
  @ParameterizedTest
  @MethodSource("ontologyAndAnySubsumptionProvider")
  void elementsAreMinImps(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException {
    MinImpEnlarger minImpEnlarger = new TrivialMinImpEnlarger();
    MinImpShrinker minImpShrinker = new ClassicalMinImpShrinker();

    HittingSetMinImpsBuilder minImpsBuilder = new HittingSetMinImpsBuilder(
        new BlackBoxMinImpBuilder(minImpEnlarger, minImpShrinker));
    elementsAreMinImps(minImpsBuilder, ontology, entailments);
  }

  @Override
  @ParameterizedTest
  @MethodSource("ontologyAndAnySubsumptionProvider")
  void completeMinImps(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException {
    MinImpEnlarger minImpEnlarger = new TrivialMinImpEnlarger();
    MinImpShrinker minImpShrinker = new ClassicalMinImpShrinker();

    HittingSetMinImpsBuilder minImpsBuilder = new HittingSetMinImpsBuilder(
        new BlackBoxMinImpBuilder(minImpEnlarger, minImpShrinker));
    completeMinImps(minImpsBuilder, ontology, entailments);
  }

  @Override
  @Test
  void doesNotModifyInputOntology() throws OWLOntologyCreationException {
    MinImpEnlarger minImpEnlarger = new ClassicalMinImpEnlarger();
    MinImpShrinker minImpShrinker = new ClassicalMinImpShrinker();

    HittingSetMinImpsBuilder minImpsBuilder = new HittingSetMinImpsBuilder(
        new BlackBoxMinImpBuilder(minImpEnlarger, minImpShrinker));
    doesNotModifyInputOntology(minImpsBuilder);
  }
}
