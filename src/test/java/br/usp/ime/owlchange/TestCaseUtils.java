/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange;

import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.HasClassesInSignature;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLSubClassOfAxiomImpl;

public class TestCaseUtils {

  final static Logger logger = LoggerFactory.getLogger(TestCaseUtils.class);

  public static OWLOntology loadOntology(OWLOntologyManager manager, String filename) {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream owlFile = classLoader.getResourceAsStream(filename);
    OWLOntology result;
    try {
      result = manager.loadOntologyFromOntologyDocument(owlFile);
    } catch (OWLOntologyCreationException e) {
      result = null;
      logger.error("Failed to load: " + filename);
    }
    return result;
  }

  public static OWLSubClassOfAxiom getAnySubsumption(OWLOntology ontology) {

    List<OWLClass> classes = ontology.classesInSignature().collect(Collectors.toList());

    OWLClass class1 = classes.get(0);
    OWLClass class2 = classes.get(classes.size() - 1);

    return new OWLSubClassOfAxiomImpl(class1, class2, new HashSet<>());
  }

  public static OWLSubClassOfAxiom getAnySubsumption(Set<OWLAxiom> ontology) {

    List<OWLClass> classes = ontology.stream().flatMap(HasClassesInSignature::classesInSignature)
        .collect(
            Collectors.toList());

    OWLClass class1 = classes.get(0);
    OWLClass class2 = classes.get(classes.size() - 1);

    return new OWLSubClassOfAxiomImpl(class1, class2, new HashSet<>());
  }
}