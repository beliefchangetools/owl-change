/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.maxnon.full;

import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HittingSetCalculator;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLAxiom;

/* Computes the best "lexicographic" MaxNons using a stratified HST implementation.
The property must be monotonic */
public class StratifiedHSMaxNonsBuilder implements MaxNonsBuilder {

  private Strata<OWLAxiom> strata;
  protected final MaxNonBuilder maxNonBuilder;


  public StratifiedHSMaxNonsBuilder(MaxNonBuilder maxNonBuilder, Strata<OWLAxiom> strata) {
    this.strata = strata;
    this.maxNonBuilder = maxNonBuilder;
  }

  @Override
  public Set<Set<OWLAxiom>> maxNons(Set<OWLAxiom> ontology,
      OntologyPropertyChecker checker) {
    StratifiedMaxNonHST hst = new StratifiedMaxNonHST(new ArrayDeque<>(), maxNonBuilder, strata, checker);
    return hst.hittingSet().getNodes();
  }

  public static class StratifiedMaxNonHST extends HittingSetCalculator<OWLAxiom> {

    protected final MaxNonBuilder maxNonBuilder;
    protected final Strata<OWLAxiom> strata;
    protected final OntologyPropertyChecker checker;
    protected final Set<OWLAxiom> ontology;
    protected Set<OWLAxiom> currentStratum;
    Integer currentBest = -1;
    Set<ImmutableSet<OWLAxiom>> validPaths;

    public StratifiedMaxNonHST(Queue<ImmutableSet<OWLAxiom>> queue, MaxNonBuilder maxNonBuilder, Strata<OWLAxiom> strata, OntologyPropertyChecker checker) {
      super(queue);
      this.maxNonBuilder = maxNonBuilder;
      this.strata = strata;
      this.checker = checker;
      this.ontology = Sets.newHashSet();
      validPaths = Sets.newHashSet();
    }

    @Override
    public HittingSetResult hittingSet() {
      for (Set<OWLAxiom> stratum : strata) {
        this.currentStratum = stratum;
        this.ontology.addAll(stratum);
        if (!validPaths.isEmpty()) {
          // Extend and add validPaths
          for (ImmutableSet<OWLAxiom> path : validPaths) {
            for (OWLAxiom newAxiom : stratum) {
              queue.add(ImmutableSet.copyOf(Sets.union(path, Sets.newHashSet(newAxiom))));
            }
          }
        } else {
          ImmutableSet<OWLAxiom> emptyPath = ImmutableSet.copyOf((Collections.emptySet()));
          queue.add(emptyPath);
        }
        hittingSetStep();
        closedPaths.clear();
      }
      return new HittingSetResult(nodes, validPaths);
    }

    @Override
    protected Optional<Set<OWLAxiom>> reusable(ImmutableSet<OWLAxiom> hittingPath) {
      Optional<Set<OWLAxiom>> result = this.nodes.parallelStream()
          .filter(e -> e.containsAll(hittingPath)).findAny();

      if (result.isPresent()) {
        if (result.get().size() == currentBest) {
          validPaths.add(hittingPath);
        }
      }

      return result;
    }

    @Override
    protected Optional<Set<OWLAxiom>> getNode(ImmutableSet<OWLAxiom> hittingPath) {

      Optional<Set<OWLAxiom>> optNode = maxNonBuilder.maxNon(this.ontology, checker, ImmutableSet.copyOf(hittingPath));

      if (optNode.isPresent()) {
        if (optNode.get().size() > currentBest) {
          currentBest = optNode.get().size();
          validPaths.clear();
        }
        if (optNode.get().size() == currentBest) {
          validPaths.add(hittingPath);
        }
      }
      return optNode;
    }

    @Override
    protected Stream<ImmutableSet<OWLAxiom>> successors(ImmutableSet<OWLAxiom> hittingPath,
        Set<OWLAxiom> node) {
      // Use only the current stratum
      return this.currentStratum.stream().filter(((Predicate<OWLAxiom>) node::contains).negate())
          .map(ImmutableSet::of)
          .map(set -> Sets.union(hittingPath, set)).map(ImmutableSet::copyOf);
    }
  }
}
