/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.minimp.full;

import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HittingSetCalculator;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Stream;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.semanticweb.owlapi.model.OWLAxiom;

/* Computes the best "lexicographic" MinImps using a stratified HST implementation.
The property must be monotonic */
public class StratifiedHSMinImpsBuilder implements MinImpsBuilder {

  private Strata<OWLAxiom> strata;
  protected final MinImpBuilder minImpBuilder;

  public StratifiedHSMinImpsBuilder(MinImpBuilder minImpBuilder, Strata<OWLAxiom> strata) {
    this.strata = strata;
    this.minImpBuilder = minImpBuilder;
  }

  @Override
  public Set<Set<OWLAxiom>> minImps(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {

    StratifiedMinImpHST hst = new StratifiedMinImpHST(new ArrayDeque<>(), minImpBuilder, strata,
        checker);

    return hst.hittingSet().getNodes();
  }

  public static class StratifiedMinImpHST extends HittingSetCalculator<OWLAxiom> {

    protected final MinImpBuilder minImpBuilder;
    protected final Strata<OWLAxiom> strata;
    protected final OntologyPropertyChecker checker;
    protected final Set<OWLAxiom> ontology;
    LexicographicScore currentBest = null;
    Set<ImmutableSet<OWLAxiom>> validPaths;
    int scoreLength = 0;

    public StratifiedMinImpHST(
        Queue<ImmutableSet<OWLAxiom>> queue, MinImpBuilder minImpBuilder,
        Strata<OWLAxiom> strata, OntologyPropertyChecker checker) {
      super(queue);
      this.minImpBuilder = minImpBuilder;
      this.strata = strata;
      this.checker = checker;
      this.ontology = Sets.newHashSet();
    }

    @Override
    public HittingSetResult hittingSet() {
      for (Set<OWLAxiom> stratum : strata) {
        scoreLength++;
        this.ontology.addAll(stratum);
        if (!validPaths.isEmpty()) {
          queue.addAll(validPaths);
        } else if (queue.isEmpty()) {
          ImmutableSet<OWLAxiom> emptyPath = ImmutableSet.copyOf((Collections.emptySet()));
          queue.add(emptyPath);
        }
        hittingSetStep();
        currentBest = null;
        closedPaths.clear();
      }
      return new HittingSetResult(nodes, validPaths);
    }

    @Override
    protected Optional<Set<OWLAxiom>> reusable(ImmutableSet<OWLAxiom> hittingPath) {
      return this.nodes.parallelStream().filter(e -> Sets.intersection(e, hittingPath).isEmpty())
          .findAny();
    }

    // TODO: be careful of concurrency and side effects of changes in the ontology
    @Override
    protected Optional<Set<OWLAxiom>> getNode(ImmutableSet<OWLAxiom> hittingPath) {
      return minImpBuilder.minImp(Sets.difference(ontology, hittingPath), checker);
    }

    @Override
    protected Stream<ImmutableSet<OWLAxiom>> successors(ImmutableSet<OWLAxiom> hittingPath,
        Set<OWLAxiom> node) {

      return node.stream().map(item -> unionAndScore(ImmutableSet.copyOf(hittingPath), item))
          .map(ImmutableSet::copyOf);

    }

    ImmutableSet<OWLAxiom> unionAndScore(ImmutableSet<OWLAxiom> path, OWLAxiom newItem) {

      Optional<Integer> optRank = strata.getRank(newItem);

      if (!optRank.isPresent()) {
        // TODO: log error
        throw new RuntimeException();
      }

      ImmutableSet<OWLAxiom> newPath = ImmutableSet.<OWLAxiom>builder().addAll(path).add(newItem)
          .build();

      LexicographicScore newScore;
      if (currentBest == null) {
        newScore = new LexicographicScore(scoreLength);
      } else {
        newScore = new LexicographicScore(currentBest);
      }
      newScore.inc(optRank.get());

      if (currentBest == null || currentBest.compareTo(newScore) > 0) {
        validPaths.clear();
        currentBest = newScore;
      }

      if (newScore == currentBest) {
        validPaths.add(newPath);
      }

      return newPath;
    }
  }

  public static class LexicographicScore implements Comparable<LexicographicScore> {

    private long[] score;

    public LexicographicScore(int scoreLength) {
      this.score = new long[scoreLength];
    }

    public LexicographicScore(LexicographicScore oldScore) {
      this.score = oldScore.getScore().clone();
    }

    public void inc(int position) {
      score[position] += 1;
    }

    @Override
    public int compareTo(@NonNull LexicographicScore other) {

      for (int i = 0; i < this.score.length; i++) {
        if (this.score[i] != other.score[i]) {
          return Long.compare(this.score[i], other.score[i]);
        }

      }
      return 0;
    }

    public long[] getScore() {
      return score;
    }
  }
}
