# Introduction

WARNING: this document is under construction and it is susceptible to
significant changes.

## Welcome!

Thank you for your interest in this project! I am sure your contribution
will be a great addition to our project. These guidelines will help you,
and the other developers, to get most of time addressing issues and making
improvements in a respectful and organized way.

Currently, we are still in the defining stages of the project, hence we
mostly support contributions on bug fixes. Also, this project is very
academic-oriented, therefore if you see any theoretical or practical problem
with the algorithms devised, do not hesitate to open an issue Not that other
types of contributions will be rejected, but a least they may take more time
to be assessed.  Unfortunately, we still are not open to structural changes
or use of certain architectural technologies such as distributed computing.

## Semantic Versioning

We will use Semantic Versioning (https://semver.org) when we settle on our
first public API.

## Tests

1. Existing tests: your contribution must pass all the unit tests provided
   to be accepted.
2. If necessary change the tests accordingly to your change.
3. New code must have a decent coverage by unit tests.

## Bug reports

When filing an issue (bug report), make sure to answer these five questions:

1. What version you are using (or commit / branch)?
2. What operating system and processor architecture are you using?
3. What did you do?
4. What was the expected outcome?
5. What was the actual outcome?

## Suggesting new features or enhancements

In this stage, this project in concerned with implementing belief base
change operations over description logic ontologies written in OWL / OWL 2.
We regard the extension to other logics, formalisms of belief change and
other areas of belief change (e.g. iterated revision) as *future work*.

If your suggestion does not fit in the aforementioned categories, please
open an issue :smile:.

## Code Review

While we still have not fixed a policy for code review, we expect to able to
respond to issues in at most a week. After a pull request, the project
managers will assess and decide whether a contribution will be accepted or
not, giving the adequate feedback to the contributor.

## Code Style

Currently we are using Google Code Style: https://google.github.io/styleguide/javaguide.html .
It is easy to find and install in IDEs such as Intelli J and Eclipse.

### Other suggestions and tips

1. To avoid committing new files without the proper Apache License 2.0
boilerplate notice, configure favourite IDE to automatically generate it for
you when you create a new file.

2. Use the *Reformat code* and *Inspect code* options in your IDE to
evaluate your code before submission.

## Commit Style

See: https://chris.beams.io/posts/git-commit/
